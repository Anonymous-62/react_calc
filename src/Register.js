import {Link,Redirect} from 'react-router-dom'
import { useState } from 'react'; 
function Register() {
    const [name, setName] = useState("");  
    const [email, setEmail] = useState("");  
    const [phone, setPhone] = useState(''); 
    const [tolog, setToLog] = useState(false);  

    const [address, setAddress] = useState(''); 
    const formSubmit = (evt) => {
        
        if(name===''){
            evt.preventDefault();
            alert("Name cannot be empty")
        }
       if(email===''){  
        evt.preventDefault();
 
           alert('Email cannot be empty')

       }
       if(email){
        const  temp =new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(email)
        if(temp!==true){
            evt.preventDefault();
            alert("Email should be like arulgetsolute@gmail.com")
        }
       }
     if(phone===''){
         evt.preventDefault()
         alert('Phone no field cannot be empty')
     }   
     if(address===''){
         evt.preventDefault()
        alert('Address field cannot be empty')
    }   

    else{
        setToLog(true)
        localStorage.setItem("name",name)
    }
       
    }
    return (
    <div className="Register">
      <form className="Register__form" onSubmit={formSubmit}>
          <h1>Register</h1>
          <input     type="text" placeholder="Enter name" className="form__name"
          value={name}
            onChange={(e)=>{
                let value = e.target.value
                value = value.replace(/[^A-Za-z]/ig, '')
                setName(value,)  
            }}

          />
          <input type="email" placeholder="Enter email" className="form__email"
          value={email}
          onChange={(e)=>{
            setEmail(e.target.value)
            
        }}
          />
          <input type="phone no" placeholder="Enter phone no" className="form__phone"
          value={phone}
          onChange={(e)=>{
            const value = e.target.value.replace(/\D/g, "");
                setPhone(value)
          }}
          />
        <textarea placeholder="Enter address"
        value={address}
        onChange={(e)=>{
            setAddress(e.target.value)
        }} 
        />
        <input type="submit" className="form__submit" value="Register"
       
        />
        <div className="form__child">
        <span>Already have an account ?</span>
        <Link to="/login">Login</Link>
        </div>
      </form>
      {tolog?<Redirect to="/Login" />:null}

    </div>
  );
}

export default Register;
