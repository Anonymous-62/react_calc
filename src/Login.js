import {Link, Redirect} from 'react-router-dom'
import { useState } from 'react'; 

function Login() {
    const [username, setUserName] = useState("");  
    const [password, setPassword] = useState("");  
    const [tohome, setToHome] = useState(false);  
    const formSubmit = (evt) => {
        if(username===''){
            evt.preventDefault();
            alert("username cannot be empty")
        }
        if(password===''){
            evt.preventDefault();
            alert("password cannot be empty")
        }
        else{
           
           let temp=localStorage.getItem("name")
           if (username===temp){
               alert("Logged in successfully")
               setToHome(true)
           }
           else{
                alert(`${username} is not exist`)
                evt.preventDefault()

           }
        }
    }
    return (
      <div className="Register">
        <form className="Register__form" onSubmit={formSubmit}>
            <h1>Login</h1>
            <input type="text" placeholder="username name" className="form__username"
            value={username}
            onChange={(e)=>{
                    let value = e.target.value
                    value = value.replace(/[^A-Za-z]/ig, '')
                    setUserName(value,)  
                
            
            }}
            />
            <input type="password" placeholder="password" className="form__password"
             value={password}
             onChange={(e)=>{
                     let value = e.target.value
                     setPassword(value)  
                 
             
             }}
            />
          <input type="submit" className="form__submit" value="Login"/>
          <div className="form__child">
        <span>Not Have An Account ?</span>
        <Link to="/Register">Register</Link>
        </div>
        </form>
        {tohome?<Redirect to="/Home" />:null}
      </div>
    );
  }
  
  export default Login;
  