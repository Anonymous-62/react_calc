
import './App.css';
import Login from './Login'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Register from './Register';
import Calc from './Calc';

function App() {
  return (
    <Router>
    <Link to="/"></Link>
    <Link to="/Home"></Link>
    <Link to="/Login"></Link>
        <Switch>
          <Route path="/Home">
          <Calc/>
          </Route>
          <Route path="/Login">
            <Login />
          </Route>
          <Route path="/">
          <Register/>
          </Route>
        </Switch>
    </Router>
    
  );
}

export default App;
